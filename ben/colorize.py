from PIL import Image, ImageOps
import numpy as np
import random
from multiprocess import Process



def main():
    image = Image.open("NightHut.jpg").convert("L")
    im = ImageOps.colorize(image, black ="blue", white ="white")
    im.show()


if __name__ == '__main__':
    p = Process(target=main, args=())
    p.start()
    p.join()

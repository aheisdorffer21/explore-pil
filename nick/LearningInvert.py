#Nicholas Jonas
#10/16/2020
#Week 1 Assignment
#The goal of this program was to create a negative/inverted image using Pillow.
import numpy as np
from PIL import Image


def main():
    #Nested for loop that changes the color values at each pixel.
    print("Hello World")
    lady = Image.open("1.jpg")
    ladyW, ladyH =lady.size
    #pix = lady.load()
    #print(type(pix))

    for i in range(ladyW): #width
        for j in range(ladyH): #height
            pixel = lady.getpixel((i,j))
            #print(pixel)
            r = pixel[0]
            g = pixel[1]
            b = pixel[2]
            newColor = (255-r,255-g,255-b) # modifying the pixel rgb values
            lady.putpixel((i,j), newColor) # Places in new rgb values
    lady.show()
    #lady.save("InvertedLady.jpg")
#main


if __name__ == "__main__":
    main()

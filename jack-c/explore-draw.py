#Jack Castiglione, Oct 14
import numpy as np
from PIL import Image, ImageDraw, ImageFont


def main():
    #Add a basic image to overlay text onto
    pic = Image.open("images/flowers-wall.jpg").convert("RGBA")

    #Make a blank image for the text, initialized to transparent text color
    draw = Image.new("RGBA", pic.size, (255,255,255,0))
    #Bring font into file
    fnt = ImageFont.load_default()

    #Create a drawing context
    d = ImageDraw.Draw(draw)

    #Make multiline, semi-opaque text
    d.multiline_text((50,50), "Hello\nWorld", font=fnt, fill=(255,255,255,255))

    #Overlay the base image and the created layers
    out = Image.alpha_composite(pic, draw)
    out.show()


if __name__ == "__main__":
    main()

import numpy as np
from PIL import Image


def main():
    #Open an image of me and convert to array
    me = Image.open("images/me.jpg").resize((512,512))
    ar = np.asarray(me)

    #Make blank array with uint8 datatype
    #Default nparray datatype of f8 is unsupported by pillow
    newAr = np.empty((512,512,3), np.uint8)

    #Set width and height values
    width = 512
    height = 512

    #Invert all numbers in the array
    for x in range(512):
        for y in range(512):
            for z in range(3):
                newAr[x][y][z] = 255 - ar[x][y][z]

    #Convert new array to image and show
    new = Image.fromarray(newAr)
    new.show()
    
if __name__ == "__main__":
    main()

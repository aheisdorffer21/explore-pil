from PIL import Image, ImageEnhance

factor = int(input("Enter an integer number for how sharp you want the image (higher means sharper): "))
enhancer = ImageEnhance.Sharpness(Image.open("NightHut.jpg"))
enhancer.enhance(factor).show(f"Sharpness {factor:f}")

"""
explore-composite.py

Author: Jakob Orel
Date: 10-11-20
CSC355 Open Source Development
Week 1

This program provides examples of PIL.Image.composite()
using masks to blend images
These examples follow closely from the examples here:
https://note.nkmk.me/en/python-pillow-composite/

Uses functions from Image, ImageDraw,
ImageFilter (blur), and ImageEnhance(sharpen).

"""

import numpy as np
from PIL import Image, ImageDraw, ImageFilter, ImageEnhance

def main():
    print("Having some fun with PIL")
    #Open the images
    squawCreek = Image.open("images/SquawCreek.png")
    bird = Image.open("images/bird.png")


    #This is Leon's example using numpy arrays but adjusted to fit my example
    width, height = squawCreek.size
    #print( f'squawcreek measures {width} x {height}')
    blackImage = Image.fromarray(np.zeros( (height, width, 3)), "RGB")
    #print( f'blackImage measures {blackImage.size[0]} x {blackImage.size[1]}')
    mask = np.full( (height, width), 255, dtype = np.uint8)

    # Change the columns every two pixels to black
    for i in range(0, width, 2):
        mask[:,i] = 0
    #for i in range(width//4, width, 2):
    #    mask[:,i] = 0

    # Changing the values of mask array to black(0) on left and right quarters
    #mask[:,0:(width//4)] = 0
    #mask[:,(3*width//4):(width)] = 0

    maskImage = Image.fromarray(mask, "L")
    compositeImage = Image.composite(squawCreek, blackImage, maskImage)
    #compositeImage.show()

    #Uniform mask image (light gray)
    uniformMask = Image.new("L", squawCreek.size, 128)

    #Creates new image with black background then draws white circle
    blackBackgroundWhiteCircle = Image.new("L", squawCreek.size, 0)
    draw = ImageDraw.Draw(blackBackgroundWhiteCircle)
    draw.ellipse((128, 128, 384, 384), fill=255)
    #blackBackgroundWhiteCircle.show()

    blackBackgroundWhiteRectangle = Image.new("L", squawCreek.size, 0)
    draw = ImageDraw.Draw(blackBackgroundWhiteRectangle)
    draw.rectangle((128, 128, 384, 384), fill=255)
    #blackBackgroundWhiteRectangle.show()

    whiteBackgroundBlackCircle = Image.new("L", squawCreek.size, 255)
    draw = ImageDraw.Draw(whiteBackgroundBlackCircle)
    draw.ellipse((128, 128, 384, 384), fill=0)
    #whiteBackgroundBlackCircle.show()

    whiteBackgroundBlackRectangle = Image.new("L", squawCreek.size, 255)
    draw = ImageDraw.Draw(whiteBackgroundBlackRectangle)
    draw.rectangle((128, 128, 384, 384), fill=0)
    #whiteBackgroundBlackRectangle.show()

    #Uses image filter to blur the mask image
    mask_blur = whiteBackgroundBlackCircle.filter(ImageFilter.GaussianBlur(10))

    #Image.composite(image1, image2, maskImage)
    # All three must be images of the same size
    # The first two images must be same type
    # The mask can be modes "1", "L", or "RGBA"
    # The two images are alpha-blended according to the value of mask
    composite = Image.composite(squawCreek, bird, mask_blur)

    # ImageEnhance has many classes including Sharpness, Brightness,
    # Contrast, and Color. The factor value determines the effect of the
    # enhance. A value of 1.0 will return the original image.
    # These effects are explored in more detail in explore-enhance.py
    enhancer = ImageEnhance.Sharpness(composite)
    composite = enhancer.enhance(2.0)

    composite.show()

if __name__ == "__main__":
    main()

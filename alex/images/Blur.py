#Alex Heisdorffer
#Week 1 homework PIL library understanding 
#Project #3 I used blur function located in the ImageFilter module 
#to make my image blury
#October 16th 2020

#I was not fully able to get this code to do what I wanted it to do 
#but I am currently working on it to try and produce the image that 
#I want I do not recieve an error message and it prints the image
#but the filter is not working and is just showing my original image with
#no filter applied

from PIL import Image 
from PIL import ImageFilter

#This is the same image file and I am using for all of my projects and
#this line opens that image so I am able to apply the filters to it 
photo = Image.open("image/20190908_151404_HDR.jpg")

#These two lines of code should apply the normal blur filter to the image
#and show that image with the filter applied
blurImage = photo.filter(ImageFilter.BLUR)
blurImage.show()

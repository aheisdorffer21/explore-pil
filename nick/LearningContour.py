#Nicholas Jonas
#10/16/2020
#Week 1 Assignment
#The goal of this program is to use the Contour image filter and blend in
#the original image to make something like Leon described in class.
from PIL import Image, ImageFilter


def main():
    #Main function that creates the contour photo and the unique one.
    print("Hello World!")

    lady = Image.open("1.jpg")
    ladyContour = lady.filter(ImageFilter.CONTOUR) #Apply Contour filter
    ladyContour.show()
    ladyContour.save("PIL_Photos/LadyContour.jpg") #Save the Image to have
    
    mask = Image.new("L", lady.size, 140) #140 for Lady, 200 for Cosmo were favorites
    img = Image.composite(lady, ladyContour, mask) # Put them together
    img.show()
    #img.save("PIL_Photos/watercolorLady.jpg")
    
#main
    

if __name__ == "__main__":
    main()

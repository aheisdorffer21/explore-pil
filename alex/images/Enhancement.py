#Alex Heisdorffer
#Week 1 homework PIL library understanding
#Project #1 Image Enhancement 
#October 16th 2020

#In this program I am taking an image that I took from when I went on a trip to 
#the boundary waters during first block last year and I am enhancing the image 
#by sharpening the image and making it look better

#One major problem that I had with this project was that everytime that I would 
#try to run the program it said that my image file was not found but it was in my 
#picture folder and it would not run but I believe that I fixed the problem and it 
#should run now

#Here I am importing libraries from PIL to be able to open and display the image 
#and imported the library to be able to enhance an image
from PIL import Image 
from PIL import ImageEnhance

#This code opens the image
photo = Image.open( "image/20190908_151404_HDR.jpg")

#Here I am defining the ImageEnhance as enhancer to help me understand a little more
enhancer = ImageEnhance.Sharpness(image)

#In this for loop I am taking the ImageEnhancer library and setting a range for the 
#enhancer to enhance my image
#At first I had a little bit of trouble understanding how this part of the code 
#worked but I believe that after I read through the module and the documentation
#I understand it 
for i in range(8):
	factor = i / 5.0
	enhancer.enhance(factor).show("Sharpness %f" % factor)

#This shows the image when run
	image.show()